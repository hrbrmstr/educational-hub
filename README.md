# educational-hub

## Project Description

Resources and tools to connect educators and students interested in exploring capstone/community service projects with D4D.

This project is just rolling out, please check out the [timeline](https://github.com/Data4Democracy/educational-hub#timeline) below for ways you get involved. We need as much educator and student input as possible to ensure we serve our target audience as best as possible.

**Slack:** [#teaching](https://datafordemocracy.slack.com/messages/teaching/)

### Students

Students interested in exploring the problems being solved, the tech being used, and the techniques being employed here in the D4D community should visit the [student_project_hub](https://github.com/Data4Democracy/educational-hub/tree/master/student_project_hub) for more information

### Educators

Educators interested in exploring connecting their students with the D4D community should visit [teaching_resources](https://github.com/Data4Democracy/educational-hub/tree/master/teaching_resources) for more information.

## Timeline

1. Brainstorming resources and information that will streamline the ability of students and educators to get involved with the D4D community. Join in on the [#teaching](https://datafordemocracy.slack.com/messages/teaching/) Slack channel.
2. Create directory structure and fill out project and subdirectory READMEs based on brainstorming.
3. Develop tools or process to keep repo up to date.

## Project Leads

* Anna Koop [@annakoop](https://datafordemocracy.slack.com/messages/@annakoop/)
* Jonathan Torrez [@jtorrez](https://datafordemocracy.slack.com/messages/@jtorrez/)

## Maintainers (people with write access)

* Project leads (see above)
* ...

## Getting started:
* **"First-timers" are welcome!** Whether you're trying to learn data science, hone your coding skills, or get started collaborating over the web, we're happy to help. *(Sidenote: with respect to Git and GitHub specifically, our [github-playground](https://github.com/Data4Democracy/github-playground) repo and the [#github-help](https://datafordemocracy.slack.com/messages/github-help/) Slack channel are good places to start.)*
* **We believe good code is reviewed code.** All commits to this repository are approved by project maintainers and/or leads (listed above). The goal here is *not* to criticize or judge your abilities! Rather, sharing insights and achievements. Code reviews help us continually refine the project's scope and direction, as well as encourage the discussion we need for it to thrive.
* **This README belongs to everyone.** If we've missed some crucial information or left anything unclear, edit this document and submit a pull request. We welcome the feedback! Up-to-date documentation is critical to what we do, and changes like this are a great way to make your first contribution to the project.
* **Get involved!** Browse our help wanted issues. All of our current discussions, proposals, and active projects are kept there and it will be the fastest way to determine how you can jump in and start helping. See if there is anything that interests you, start commenting here or in the [Slack channel](https://datafordemocracy.slack.com/messages/teaching/), and you can start contributing right away!

## Skills
* Coding in Python, R, SQL
* Web scraping and integration, REST APIs
* Natural language processing, data mining, classification, social network mapping...
* Other skills you may learn or already posses relevant to this project.
