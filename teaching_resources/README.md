# Teaching Resources

Resources for teachers and educators interested in having their students work with D4D.

Directories can group together related resources. Tutorial exercises, suggested structures, support material all welcome.
