# Icebreaker Software Engineering Exercise(s)

## Learning Objective: 
Hands-on experience creating readable code.

## Preparation
Prepare recipe cards with mini programming assignments on them, at least one for each student. See below for ideas.

## In-class

* Distribute one recipe card to each student. Provide blank paper if necessary.

* Instruct students to write (on paper, in language/pseudocode of choice) a function for the task they have been given. Do not tell them what will be done with it.
* Take in written functions, shuffle, redistribute. Give them a few minutes to read over the code
* Students are to answer questions: 
** What does the function you received do? 
** Can you tell why it does it that way? 
** Could it be clearer? 
** Enough comments/too many? 
** Easy/hard to read? 
** Sensable variable/function names?

* Add docstring-esque comments to the function and (if necessary) change the function name.

## Extensions
* Have students extend each other’s code for a second task
* Have students write test cases for the task before/after 
* Have them actually write the code for functions that are meant to work together
* Repeat as a pair-programming exercise (with more advanced functions). Do the swap, see if pair-programming inherently improves readability

## Random thoughts
* Advanced version of the exercise: give a dummy function name so that the code itself (and/or comments) must be self-explanatory. 
* Could save these papers for future exercises.

## Programming tasks brainstorm:
* password parser
* pull email data from xml file
* logsumexp (?)
* validate file path (exists, has content)
* create a mapping between old and new classroom codes
* check a string for a guessed letter (i.e. hangman)



