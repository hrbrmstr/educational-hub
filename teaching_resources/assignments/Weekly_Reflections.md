# Weekly Reflection assignment

## Objective
* Gain fluency in processing and reflection
* Practice setting and evaluating goals.
* Let the instructor/TA know what's going on among the teams.

## Structure
Each week the students are required to submit a short post to a private forum. Posts are only viewable by the student themselves and instructor/TA. Moodle has a forum type for this, but paper would work as well. Privacy is important.

Students are prompted with the following questions
1. What have you/your team been working on? Gotten done?
2. What are your team's goals for this week?
3. What are your personal goals for this week?
4. Any recent triumphs? Something gone particularly well? Completed something, found or fixed a bug?
5. Any particular difficulties or concerns?

## Marking
Posts are marked strictly with respect to thoughtfulness and completeness re: prompt questions. Comments on the quality of the goals or accomplishments may be provided but not factor in to the marking. The quality of the students' work should be evaluated separately through assessment of milestone documents or submitted code.  

A side benefit of this assignment is that it allows for some individual differentiation when the bulk of the mark is team-based.

Generally speaking this is most successful if reflections form a small but non-trivial portion of the final grade. It is important to have a deadline set when reflections must be submitted by.
