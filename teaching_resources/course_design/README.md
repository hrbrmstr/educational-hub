# Course Design Resources

This is for holistic course resources. 
If you are providing full course resources, please create an appropriately-named directory. # TODO: naming scheme?


Feel free to include as much or as little as you can, but here are some guidelines.

Required information
* What are the prerequisites for the course? Not (just) in terms of the previous courses at your institution, but the knowledge you expect of the students.
* What are the objectives of the course? This may be specifically the calendar description, or a list of learning objectives, or a general overview of the skills/knowledge the students are expected to get out of it.
* What scope is the course? Is this full time, part time; a lab and lecture or a free-form project-based assignment?

Optional information
* Course syllabus
* Grade breakdown
* Lecture topics
* Feedback from students and instructors who have run the course
* Grading rubrics
* Assignment details
* Lecture resources
* Recommended textbooks and other resources


